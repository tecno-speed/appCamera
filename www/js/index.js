function tirarFoto() {
    var cameraOptions = {
        quality: 90,
        destinationType: 0,
        allowEdit: true,
        targetWidth: 500,
        targetHeight: 250
    }

    navigator.camera.getPicture(cameraSuccess, cameraError, cameraOptions)
}

function cameraSuccess(imgData) {
    var imgAtual = "data:image/jpeg;base64," + imgData;
    document.getElementById('foto').src = imgAtual;
}

function cameraError(msg) {
    alert(msg)
}
